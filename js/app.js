//var app = angular.module('CV', ['angular-md5','ui.bootstrap']);
'use strict';

var app = angular.module('CV', ['angular-md5','ui.swiper','ngAnimate']);
var url = "../../cartelera_backend_laravel/public/";


/**
 * Esta clase contiene el las funciones de consulta al Back-end.
 * Se envuelve luego en un wrapper RemoteSourceProvider para ser 
 * inyectada como un Provider en cualquier controlador.
 */
function RemoteResource($http, $q) {
	/**
	 * Recupera la configuración de la cartelera
	 */
	this.get = function() {
		var defered=$q.defer();
		var promise=defered.promise;
		
		$http({
			method: 'GET',
			url: url + '?ruta=api/config/cv'
		}).then(function(data, status, headers, config) {
			defered.resolve(data);
		})
		
		return promise;
	}

	/**
	 * Recupera del Back-end la lista de servicios.
	 */
	this.servicios = function (salas,url1) {
		var defered = $q.defer();
		var promise = defered.promise;
		
		if(salas == "0") {
			var _url = url1 + '?ruta=api/servicios';
		} else {
			var _url = url1 + '?ruta=api/servicios_por_sala&salas=' + salas;
		}
		
		
		
		$http({
			method: 'GET',
			url: _url
		}).then(function(data, status, headers, config) {
			defered.resolve(data);
		});
		return promise;
	}
	
	/*
	this.servicios = function() {
		var defered=$q.defer();
		var promise=defered.promise;
		
		$http({
			method: 'GET',
			url: url + 'api/servicios'
		}).then(function(data, status, headers, config) {
			defered.resolve(data);
		});
		
		return promise;
	}
	*/
	

	/**
	 * Verifica el estado de la licencia de la cartelera.
	 */
	this.licencia = function() {
		var defered=$q.defer();
		var promise=defered.promise;

		$http({
			method: 'GET',
			url: url + '?ruta=api/licencia&product_id=35'
		}).then(function(data, status, headers, config) {
			defered.resolve(data);
		});

		return promise;
	}
}

/**
 * Wrapper para envolver la clase RemoteSource en un provider
 */
function RemoteResourceProvider() {
	var _baseUrl;
	this.setBaseUrl = function() {
		_baseUrl = url;
	}
	this.$get = ['$http','$q',function($http,$q) {
			return new RemoteResource($http,$q, _baseUrl);
	}];
}

app.provider("remoteResource", RemoteResourceProvider);


/**
 * Directiva para envolver el diálogo
 */
app.directive('modalDialog', function($window, $templateCache, $compile, $http) {
	return {
		restrict: 'EA',
		scope: {
			show: '=',
			modalUser: '=',
			saveUser: '&',
			templateUser: '@',
		},
		replace: true, // Replace with the template below
		//transclude: true, // we want to insert custom content inside the directive
		link: function(scope, element, attrs) {

			$http.get(scope.templateUser, {cache: $templateCache}).then(function(tplContent){
								element.replaceWith($compile(tplContent.data)(scope));        
								
								//element.replaceChild($compile(tplContent)(scope));                
							});
							
			scope.dialogStyle = {};
			if (attrs.width) {
				scope.dialogStyle.width =  attrs.width  ;
				scope.dialogStyle.left =  attrs.width  ;
			}
			if (attrs.height) {
				scope.dialogStyle.height = ( ( 100 - attrs.height ) / 2	 )+ '%';
				scope.dialogStyle.top = ( ( 100 - attrs.height ) / 2	 ) + '%';
			}
				
			scope.hideModal = function() {
				scope.show = false;
			};

			scope.clone = function(obj) {
				if (obj === null || typeof obj !== 'object') {
						return obj;
				}
				var temp = obj.constructor(); // give temp the original obj's constructor
				for (var key in obj) {
						temp[key] = scope.clone(obj[key]);
				}
				return temp;
			};

			var tempUser = scope.clone(scope.modalUser);
			
			scope.save = function() {
				scope.saveUser(scope.modalUser);
				scope.show = false;
			};
			
			scope.cancel = function() {
				scope.modalUser = scope.clone(tempUser);
				scope.show = false;
			};
		}
		//template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
		//templateUrl: 'my-customer.html'
		//templateUrl: scope.templateUser
	};
});

app.directive('onLongPress', function($timeout) {
	return {
		restrict: 'A',
		link: function($scope, $elm, $attrs) {
			$elm.bind('touchstart', function(evt) {
				// Locally scoped variable that will keep track of the long press
				$scope.longPress = true;

				// We'll set a timeout for 600 ms for a long press
				$timeout(function() {
					if ($scope.longPress) {
						// If the touchend event hasn't fired,
						// apply the function given in on the element's on-long-press attribute
						$scope.$apply(function() {
							$scope.$eval($attrs.onLongPress)
						});
					}
				}, 600);
			});

			$elm.bind('touchend', function(evt) {
				// Prevent the onLongPress event from firing
				$scope.longPress = false;
				// If there is an on-touch-end function attached to this element, apply it
				if ($attrs.onTouchEnd) {
					$scope.$apply(function() {
						$scope.$eval($attrs.onTouchEnd)
					});
				}
			});
		}
	};
})

app.controller('cofigCtrl', ['$scope', 'remoteResource', '$location','$rootScope', function ($scope, remoteResource, $location,$rootScope) {	
		
	$scope.url=url;
	$scope.config = {};
	$scope.fecha = new Date();
	
	$scope.license_alert = false;

	remoteResource.get().then(function(seguro) {
		
		$scope.configCV = seguro.data.configCV;
		
		$scope.fondos = seguro.data.fondos;
		$scope.publicidades = seguro.data.publicidades;
		$rootScope.banners_pri = seguro.data.banners;
		$scope.currentBanner = url+seguro.data.banners[0];
		
	}, function(status) {
		
	});
	
	// remoteResource.licencia().then(function(licencia) {
	// 	$scope.licencia = licencia.data;
	// 	console.log('LICENCIA',$scope.licencia);
	// 	if ($scope.licencia.status == "Inactive") {
	// 		$scope.license_alert = true;
	// 		$scope.license_message = "La licencia está inactiva";
	// 	} else if ($scope.licencia.status == "Active") {
	// 		$scope.license_alert = false;
	// 		$scope.license_message = "Licencia activa";
	// 	} else {
			
	// 		$scope.license_alert = true;
	// 		$scope.license_message = "No hay licencia configurada para este producto";
	// 	}

	// }, function(status) {
		
	// 	$scope.license_alert = true;
	// 	$scope.license_message = "No hay licencia configurada para este producto";
	// });
}]);

app.controller('servicioCtrl', ['$scope', '$window','md5','remoteResource','$http' ,'$interval','$rootScope',function ($scope, $window ,md5,remoteResource,$http,$interval,$rootScope) {
	$rootScope.servidor = {};
	
	// $rootScope.slidebaners = {};
	$rootScope.intervalbanner = 2000;
	$rootScope.banner1 = '';
	$scope.url = url;
	$scope.config = {};
	$scope.fecha = new Date();
	$scope.CurrentDate = new Date();
	var day = $scope.fecha.getDate()+1;
	var month = $scope.fecha.getMonth()+1;
	var year = $scope.fecha.getFullYear();
	
	$scope.fecha_new = $scope.CurrentDate.getFullYear()+"-"+(($scope.CurrentDate.getMonth()+1)<10?'0':'')+($scope.CurrentDate.getMonth()+1)+"-"+(($scope.CurrentDate.getDate()+1)<10?'0':'')+($scope.CurrentDate.getDate()+1);
	// console.log('LINEA 272',$scope.fecha);
	// console.log('LINEA 273',$scope.fecha_new);
	$scope.onInit = function (swiper) {
		
		$scope.swiper = swiper;
		$scope.swiper.params.autoplay = $rootScope.intervalbanner;
	}
			remoteResource.get().then(function(seguro) {
				
				$scope.configCV = seguro.data.configCV;
				var tiempo =$scope.configCV.tiempo_banner;
				tiempo =tiempo * 1000;
				$rootScope.intervalbanner = tiempo;

				$scope.banner = seguro.data.banners;
				var banners =angular.fromJson($scope.banner);
				$scope.slidebaners = banners;
				console.log('ROOT BANNER 1',$scope.slidebaners);
			});				

	$http.get(url+'?ruta=api/config/ni').then(function(res){
		
		$scope.configNI = res.data.configNI;
		var color =$scope.configNI.color_back;
		$scope.configNI.color_cont = chroma(color).darken().hex();
		
	});	

	$scope.logo = 'images/empresa/logo.png';
	callConfig();
	function callConfig(){
		remoteResource.get().then(function(seguro) {
			
			$scope.configCV = seguro.data.configCV;
			$rootScope.servidor = seguro.data.configCV;
			var tiempo =$scope.configCV.tiempo_banner;
			var back;
			tiempo =tiempo * 1000;
			$rootScope.intervalbanner = tiempo;
			$scope.swiper.params.autoplay = $rootScope.intervalbanner;
			var color =$scope.configCV.color_back;
			$scope.configCV.color_cont = chroma(color).darken().hex();

			console.log('Configuracion',$rootScope.servidor);
			$scope.fondos = seguro.data.fondos;
			$scope.publicidades = seguro.data.publicidades;
			
			$scope.banner = seguro.data.banners;
			var banners =angular.fromJson($scope.banner);
			$scope.slidebaners = banners;
						
			console.log('ROOT BANNER 2',$scope.slidebaners);

			$scope.fondo_servicio = url+seguro.data.fondo_servicio[0];
			if(seguro.data.fondo_servicio.length==0){
					//$scope.myStyle={'background-color': $scope.configCV.color_back}
	
					back=$scope.configCV.color_back;
					
					back=back+10;
					
	
					$scope.myStyle2={'background-color': back}
			}else{
				// $scope.myStyle={'background-image': 'url('+$scope.fondo_servicio +')'}
			}
			$scope.logo = 'images/empresa/logo.png';
		},function(status) {
			
		});

		$http.get(url+'?ruta=api/config/ni').then(function(res){
			
			$scope.configNI = res.data.configNI;
			var color =$scope.configNI.color_back;
			$scope.configNI.color_cont = chroma(color).darken().hex();
			
		});	
	}


	$scope.servicios = {};
	$scope.modalShown = false;
	$scope.modalShown2 = false;
	$scope.user = {name:"", surname:"", shortKey:url};
	$scope.userMod = {};

	callAtInterval($rootScope.servidor);
		
	 var promise = $interval(function() 
	 { 
		callAtInterval($rootScope.servidor);
		callConfig();
	 }, 
	 10000);

	function callAtInterval(serv) {
		// console.log(serv);
		var urlServ='';
		if(serv.tipo_servidor == 'online'){
			urlServ = serv.servidor_url+'cartelera_backend_laravel/public/';
			
		}else{
			if(serv.tipo_servidor == 'local'){
				urlServ = 'http://'+serv.servidor_ip+'/cartelera_backend_laravel/public/';
			}else{
				urlServ = url;
			}
		}

		if(serv.tipo_servidor == 'local' || serv.tipo_servidor == 'online'){
			$http.get(urlServ+'?ruta=api/config/cv')
				.then(function(res){
					
					console.log('CONFIG REMOTO',res);
					$scope.configRemoto = res.data.configCV;
				});
		}
		
		console.log('SALAS',serv.salas_id);
		if(serv.salas_id){
			var arr = serv.salas_id.split(",");
			// console.log('SALAS SEPARADAS',arr);
		}
				
		console.log('URL',urlServ,arr);
		remoteResource.servicios(arr,urlServ).then(function(data) {
			
			console.log('SERVICIOS ',data.data);

			if(data.data.length>=5){
				$scope.sco_new=false;
				$scope.hideDiv = true;
				$scope.ctServicio=false;
				$scope.cvSrvicios=false;	
				$scope.ctImagen=false;	
				$scope.ctInfo=false;	
				$scope.hidecarr = {height: 0, overflow: "hidden"};	
				$scope.hidebaner = {height: 0, overflow: "hidden"};
				$scope.hidebaner2 = {width: 0, height: 0, overflow: "hidden"};
			}
			if(data.data.length==4){
				$scope.sco_new=false;
				$scope.hideDiv = false;
				$scope.ctServicio=false;
				$scope.cvSrvicios=false;	
				$scope.ctImagen=false;	
				$scope.ctInfo=false;
				$scope.cvpie=true;	
				$scope.cvpie2=false;	
				$scope.cvpie1=false;
				$scope.ctbanner=false;
				$scope.ctbanner=true;	
				$scope.hidecarr = {height: 0, overflow: "hidden"};	
				$scope.hidebaner = {};
				$scope.hidebaner2 = {};
			}
			if(data.data.length==3){
				$scope.sco_new=true;
				$scope.hideDiv = false;
				$scope.ctServicio=false;
				$scope.cvSrvicios=true;	
				$scope.ctImagen=true;	
				$scope.ctInfo=true;
				$scope.cvpie=false;	
				$scope.cvpie2=false;	
				$scope.cvpie1=false;
				$scope.ctbanner=false;	
				$scope.hidecarr = {height: 0, overflow: "hidden"};	
				$scope.hidebaner = {};	
				$scope.hidebaner2 = {};		
			}
			if(data.data.length==2){
				$scope.sco_new=true;
				$scope.hideDiv = false;
				$scope.ctServicio=false;
				$scope.cvSrvicios=true;	
				$scope.ctImagen=true;	
				$scope.ctInfo=true;	
				$scope.cvpie2=true;	
				$scope.cvpie1=false;	
				$scope.ctbanner=false;	
				$scope.hidecarr = {height: 0, overflow: "hidden"};	
				$scope.hidebaner = {};		
				$scope.hidebaner2 = {};
			}
			if(data.data.length==1){
				$scope.sco_new=true;
				$scope.hideDiv = false;
				$scope.ctServicio=false;
				$scope.cvSrvicios=true;	
				$scope.ctImagen=true;	
				$scope.ctInfo=true;	
				$scope.cvpie1=true;	
				$scope.ctbanner=false;	
				$scope.hidecarr = {height: 0, overflow: "hidden"};	
				$scope.hidebaner = {};
				$scope.hidebaner2 = {};
					
			}
			if(data.data.length==0){
				//$scope.hideDiv = true;
				$scope.ctServicio=false;
				$scope.cvSrvicios=false;	
				$scope.ctImagen=false;	
				$scope.ctInfo=false;	
				$scope.hidecarr = {};
				$scope.hidebaner = { height: 0, overflow: "hidden"};
				$scope.hidebaner2 = {width: 0, height: 0, overflow: "hidden", margin: 0};
			}	
			angular.forEach(data.data, function(value, key){		
				value = Object.defineProperty(value, "imagen1", { writable: true });
				if (value.sala_id!=-10 && value.sala_id!=99 && value.sala_id!=100 && value.sala_id!=0){
					switch($scope.configCV.imagen1){
						case 'foto': value.imagen1 = urlServ+value.foto; break;
						case 'icono': value.imagen1 = urlServ+'images/salas/'+value.sala.icono; break;	
						case 'cruz': if(value.religion != null){ value.imagen1 = urlServ+'images/cruces/'+value.religion.cruz;}; break;
						case 'simbolo': if(serv.tipo_servidor=='local' || serv.tipo_servidor=='online'){value.imagen1 = urlServ+'images/simbolos/'+$scope.configRemoto.simbolos[value.sala.id];}else{value.imagen1 = urlServ+'images/simbolos/'+$scope.configCV.simbolos[value.sala.id];}; break;
						case 'QR1': value.imagen1 = urlServ+'images/servicios/qrcodes/'+value.qrcode1; break;
						case 'QR2': value.imagen1 = urlServ+'images/servicios/qrcodes/'+value.qrcode2; break;
					}
					value = Object.defineProperty(value, "imagen2", { writable: true });
					//switch(seguro.configCV.imagen2){
					switch($scope.configCV.imagen2){	
						case 'foto': value.imagen2 = urlServ+value.foto; break;
						case 'icono': value.imagen2 = urlServ+'images/salas/'+value.sala.icono; break;
						case 'cruz': if(value.religion != null){ value.imagen2 = urlServ+'images/cruces/'+value.religion.cruz;}; break;
						case 'simbolo': if(serv.tipo_servidor=='local'||serv.tipo_servidor=='online'){value.imagen2 = urlServ+'images/simbolos/'+$scope.configRemoto.simbolos[value.sala.id];}else{value.imagen2 = urlServ+'images/simbolos/'+$scope.configCV.simbolos[value.sala.id];}; break;
						case 'QR1': value.imagen2 = urlServ+'images/servicios/qrcodes/'+value.qrcode1; break;
						case 'QR2': value.imagen2 = urlServ+'images/servicios/qrcodes/'+value.qrcode2; break;
					}
					value = Object.defineProperty(value, "imagen3", { writable: true });
					//switch(seguro.configCV.imagen3){
					switch($scope.configCV.imagen3){	
						case 'foto': value.imagen3 = urlServ+value.foto; break;
						case 'icono': value.imagen3 = urlServ+'images/salas/'+value.sala.icono; break;
						case 'cruz': if(value.religion != null){ value.imagen3 = urlServ+'images/cruces/'+value.religion.cruz;}; break;
						case 'simbolo': if(serv.tipo_servidor=='local'||serv.tipo_servidor=='online'){value.imagen3 = urlServ+'images/simbolos/'+$scope.configRemoto.simbolos[value.sala.id];}else{value.imagen3 = urlServ+'images/simbolos/'+$scope.configCV.simbolos[value.sala.id];}; break;
						case 'QR1': value.imagen3 = urlServ+'images/servicios/qrcodes/'+value.qrcode1; break;
						case 'QR2': value.imagen3 = urlServ+'images/servicios/qrcodes/'+value.qrcode2; break;
					}
					value = Object.defineProperty(value, "imagen4", { writable: true });
					//switch(seguro.configCV.imagen4){
					switch($scope.configCV.imagen4){	
						case 'foto': value.imagen4 = urlServ+value.foto; break;
						case 'icono': value.imagen4 = urlServ+'images/salas/'+value.sala.icono; break;
						case 'cruz': if(value.religion != null){ value.imagen4 = urlServ+'images/cruces/'+value.religion.cruz;}; break;
						case 'simbolo': if(serv.tipo_servidor=='local'||serv.tipo_servidor=='online'){value.imagen4 = urlServ+'images/simbolos/'+$scope.configRemoto.simbolos[value.sala.id];}else{value.imagen4 = urlServ+'images/simbolos/'+$scope.configCV.simbolos[value.sala.id];}; break;
						case 'QR1': value.imagen4 = urlServ+'images/servicios/qrcodes/'+value.qrcode1; break;
						case 'QR2': value.imagen4 = urlServ+'images/servicios/qrcodes/'+value.qrcode2; break;
					}
				}else{
					value.imagen1 = urlServ+'images/cruces/'+value.religion.cruz; 
					value.imagen2 =  urlServ+value.foto;
					value.imagen4 = urlServ+'images/cruces/'+value.religion.cruz; 
					
				}
			});

			$scope.servicios = data.data;

			// if($rootScope.banner1 != $rootScope.banners_pri){
			// 	$rootScope.banner1 = $rootScope.banners_pri;
			// }
			// console.log('Root banner 3',$rootScope.banners_pri);
			if($rootScope.banner1 != $scope.banner){
				console.log('msj cambio')
			}

			// var banners =angular.fromJson($scope.banner);
			// console.log('Root banner 3',banners);
			// $scope.slidebaners =banners;

			$scope.contenedor1 = data.salida;
		}, function(status) {
		  
		});
	};

		$scope.toggleModal = function() {
			$scope.modalShown = !$scope.modalShown;
		};

		$scope.toggleModal2 = function() {
			$scope.modalShown2 = !$scope.modalShown2;
		};

		$scope.saveUser = function(usr) {
			$scope.userMod = usr;
			$scope.userMod.surname=md5.createHash($scope.userMod.name);
			//$window.alert( $scope.userMod.name+' | '+$scope.userMod.surname);
			var pass=$scope.userMod.name;
			var pass2=$scope.userMod.surname;

			$scope.userMod.surname="";$scope.userMod.name;
			$scope.userMod.name="";
			$http.post(url+'auth/login1', {'password': pass, 'password_new': pass2})
			.then(function(data, status, headers, config) {
 
							$window.location.href= url+'/servicios';
			});				
		};
		
		$scope.itemOnLongPress = function() {
			
		};

		$scope.itemOnTouchEnd = function() {

		};

	mueveReloj();
	function mueveReloj(){ 
	   	var momentoActual = new Date();
	   	var hora = (momentoActual.getHours() < 10?"0":"")+momentoActual.getHours(); 
	   	var minuto = (momentoActual.getMinutes() < 10?"0":"")+momentoActual.getMinutes();

	   	var horaImprimible = hora + ":" + minuto;

	   	$scope.hora = horaImprimible;
	} 

	var promise = $interval(function() { 
		mueveReloj();
	}, 1000); //1 Segundo
}]);

app.controller("CarouselDemoCtrl", ['$scope','remoteResource','$rootScope','$interval',function($scope,remoteResource,$rootScope,$interval){
	
 //$scope.slides = ["uno", "dos", "tres"];
 	$rootScope.intervalbanner = 2000;
	 
	$scope.onInit = function (swiper) {
		
		$scope.swiper = swiper;
		$scope.swiper.params.autoplay = $rootScope.intervalbanner;
		//$scope.swiper.params.speed = "2000";
	}
	
	remoteResource.get().then(function(data) {
		
		var tiempo =data.data.configCV.tiempo_publi;
		tiempo =tiempo * 1000;
		$rootScope.myInterval = tiempo;
		// $scope.swiper.params.autoplay = $rootScope.myInterval;
		var publicidad =angular.fromJson(data.data.publicidades);
		
		$scope.slides=publicidad;

	});

	$interval(function(){
		remoteResource.get().then(function(data) {
			
			var tiempo =data.data.configCV.tiempo_publi;
			tiempo =tiempo * 1000;
			$rootScope.myInterval = tiempo;
			$scope.swiper.params.autoplay = $rootScope.myInterval;
			var publicidad =angular.fromJson(data.data.publicidades);
			
			$scope.slides=publicidad;

		});
	},10000);
}]);
